insert into articles(id,title,content,priority,type,visible,created_at) values(10001, '汪礼衽', '汪礼衽老师讲授过的课程有：现代分析基础。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10002, '李良攀', '李良攀老师讲授过的课程有：现代分析基础、数学分析。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10003, '殷浩', '殷浩老师讲授过的课程有：现代分析基础、数学分析。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10004, '戎锋', '戎锋老师讲授过的课程有：现代分析基础。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10005, '乃兵', '乃兵老师讲授过的课程有：数学分析。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10006, '金石', '金石老师讲授过的课程有：数学分析。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10007, '武同锁', '武同锁老师讲授过的课程有：代数、线性代数、抽象代数。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10008, '冯卫国', '冯卫国老师讲授过的课程有：概率论与数理统计。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10009, '陈新兴', '陈新兴老师讲授过的课程有：概率论及其应用。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10010, '吴耀琨', '吴耀琨老师讲授过的课程有：概率论及其应用、图论。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10011, '韩东', '韩东老师讲授过的课程有：概率论。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10012, '仇璘', '仇璘老师讲授过的课程有：科学与工程计算。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10013, '谢建利', '谢建利老师讲授过的课程有：科学与工程计算。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10014, '王增琦', '王增琦老师讲授过的课程有：科学工程与计算。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10015, '马红孺', '马红孺老师讲授过的课程有：大学物理。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10016, '卢文发', '卢文发老师讲授过的课程有：大学物理。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10017, '胡其图', '胡其图老师讲授过的课程有：大学物理。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10018, '李晟', '李晟老师讲授过的课程有：物理学引论。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10019, '周玲玲', '周玲玲老师讲授过的课程有：模拟电路、电路与电子学。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10020, '陈平', '陈平老师讲授过的课程有：数字逻辑。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10021, '翁惠玉', '翁惠玉老师讲授过的课程有：计算机科学导论、程序设计、数据结构、计算机网络。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10022, '梁阿磊', '梁阿磊老师讲授过的课程有：计算机科学导论及讨论课、计算机组成与系统结构。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10023, '程国英', '程国英老师讲授过的课程有：程序设计。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10024, '窦延平', '窦延平老师讲授过的课程有：数据结构。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10025, '沈恩绍', '沈恩绍老师讲授过的课程有：基础集论、集合论与数理逻辑、数理逻辑、计算理论、应用逻辑。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10026, '陈翌佳', '陈翌佳老师讲授过的课程有：算法分析与设计、图论与组合。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10027, '朱洪', '朱洪老师讲授过的课程有：算法设计基础。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10028, '陈晓敏', '陈晓敏老师讲授过的课程有：图论与组合。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10029, '傅育熙', '傅育熙老师讲授过的课程有：可计算性理论、并行理论、程序语言理论、计算理论。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10030, '过敏意', '过敏意老师讲授过的课程有：编译原理。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10031, '沈耀', '沈耀老师讲授过的课程有：编译原理。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10032, '张冬茉', '张冬茉老师讲授过的课程有：编译原理。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10033, '尤晋元', '尤晋元老师讲授过的课程有：操作系统。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10034, '邹恒明', '邹恒明老师讲授过的课程有：操作系统、数据库系统原理。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10035, '朱其立', '朱其立老师讲授过的课程有：数据库系统原理。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10036, '陆朝俊', '陆朝俊老师讲授过的课程有：数据库系统原理。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10037, '马范援', '马范援老师讲授过的课程有：计算机网络。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10038, '朱燕民', '朱燕民老师讲授过的课程有：计算机网络。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10039, '张丽清', '张丽清老师讲授过的课程有：人工智能、统计学习与推理。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10040, '吕宝粮', '吕宝粮老师讲授过的课程有：人工智能、神经网络理论与应用。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10041, '饶若楠', '饶若楠老师讲授过的课程有：面向对象方法与技术。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10042, '曹珍富', '曹珍富老师讲授过的课程有：密码学基础。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10043, '林德璋', '林德璋老师讲授过的课程有：软件工程。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10044, '俞勇', '俞勇老师讲授过的课程有：基础实践、科研实践、学子讲坛、科研专题。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10045, '张尧弼', '张尧弼老师讲授过的课程有：微机与接口技术。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10046, '骆源', '骆源老师讲授过的课程有：信息论与编码。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10047, '邓玉欣', '邓玉欣老师讲授过的课程有：程序语言理论。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10048, '伍民友', '伍民友老师讲授过的课程有：高级计算机系统结构。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10049, '王英林', '王英林老师讲授过的课程有：机器学习。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10050, '姚天昉', '姚天昉老师讲授过的课程有：基于Internet的信息获取技术、自然语言理解。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10051, '来学嘉', '来学嘉老师讲授过的课程有：密码工程。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10052, '苑波', '苑波老师讲授过的课程有：生物信息学。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10053, '卢宏涛', '卢宏涛老师讲授过的课程有：数字图象处理。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10054, '马利庄', '马利庄老师讲授过的课程有：计算机图形学。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());
insert into articles(id,title,content,priority,type,visible,created_at) values(10055, 'John Edward Hopcroft', 'John Edward Hopcroft讲授过的课程有：面向计算机科学的数学基础。请评论本文，以提供该任课教师语录或您对该教师的评价。谢谢！', 7654321, 'post', 0, now());

<table>
<tr>
<td><a href="/article/10001">汪礼衽</a></td>
<td><a href="/article/10002">李良攀</a></td>
<td><a href="/article/10003">殷浩</a></td>
<td><a href="/article/10004">戎锋</a></td>
<td><a href="/article/10005">乃兵</a></td>
</tr>
<tr>
<td><a href="/article/10006">金石</a></td>
<td><a href="/article/10007">武同锁</a></td>
<td><a href="/article/10008">冯卫国</a></td>
<td><a href="/article/10009">陈新兴</a></td>
<td><a href="/article/10010">吴耀琨</a></td>
</tr>
<tr>
<td><a href="/article/10011">韩东</a></td>
<td><a href="/article/10012">仇璘</a></td>
<td><a href="/article/10013">谢建利</a></td>
<td><a href="/article/10014">王增琦</a></td>
<td><a href="/article/10015">马红孺</a></td>
</tr>
<tr>
<td><a href="/article/10016">卢文发</a></td>
<td><a href="/article/10017">胡其图</a></td>
<td><a href="/article/10018">李晟</a></td>
<td><a href="/article/10019">周玲玲</a></td>
<td><a href="/article/10020">陈平</a></td>
</tr>
<tr>
<td><a href="/article/10021">翁惠玉</a></td>
<td><a href="/article/10022">梁阿磊</a></td>
<td><a href="/article/10023">程国英</a></td>
<td><a href="/article/10024">窦延平</a></td>
<td><a href="/article/10025">沈恩绍</a></td>
</tr>
<tr>
<td><a href="/article/10026">陈翌佳</a></td>
<td><a href="/article/10027">朱洪</a></td>
<td><a href="/article/10028">陈晓敏</a></td>
<td><a href="/article/10029">傅育熙</a></td>
<td><a href="/article/10030">过敏意</a></td>
</tr>
<tr>
<td><a href="/article/10031">沈耀</a></td>
<td><a href="/article/10032">张冬茉</a></td>
<td><a href="/article/10033">尤晋元</a></td>
<td><a href="/article/10034">邹恒明</a></td>
<td><a href="/article/10035">朱其立</a></td>
</tr>
<tr>
<td><a href="/article/10036">陆朝俊</a></td>
<td><a href="/article/10037">马范援</a></td>
<td><a href="/article/10038">朱燕民</a></td>
<td><a href="/article/10039">张丽清</a></td>
<td><a href="/article/10040">吕宝粮</a></td>
</tr>
<tr>
<td><a href="/article/10041">饶若楠</a></td>
<td><a href="/article/10042">曹珍富</a></td>
<td><a href="/article/10043">林德璋</a></td>
<td><a href="/article/10044">俞勇</a></td>
<td><a href="/article/10045">张尧弼</a></td>
</tr>
<tr>
<td><a href="/article/10046">骆源</a></td>
<td><a href="/article/10047">邓玉欣</a></td>
<td><a href="/article/10048">伍民友</a></td>
<td><a href="/article/10049">王英林</a></td>
<td><a href="/article/10050">姚天昉</a></td>
</tr>
<tr>
<td><a href="/article/10051">来学嘉</a></td>
<td><a href="/article/10052">苑波</a></td>
<td><a href="/article/10053">卢宏涛</a></td>
<td><a href="/article/10054">马利庄</a></td>
<td><a href="/article/10055">John Edward Hopcroft</a></td>
</tr>
</table>
